import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { MyNavBar } from "./components/MyNavBar";
import { Banner } from "./components/Banner";
import { Skills } from "./components/Skills";
import { MyProjects } from "./components/Projects";
import { Footer } from "./components/Footer";

function App() {
  return (
    <div className="App">
      <MyNavBar />
      <Banner />
      <Skills />
      <MyProjects />
      <Footer />
    </div>
  );
}

export default App;
